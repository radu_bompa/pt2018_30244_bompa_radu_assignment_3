package model;

/**
 * Created by Radu Bompa
 * 5/2/2018
 * A class representing a client
 */
public class Customer {
	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
