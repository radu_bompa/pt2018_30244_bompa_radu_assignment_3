package model;

/**
 * Created by Radu Bompa
 * 5/3/2018
 * An item in an order
 */
public class OrderItem {
	private int id;
	private int clientOrder;
	private int productId;
	private double quantity;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientOrder() {
		return clientOrder;
	}

	public void setClientOrder(int clientOrder) {
		this.clientOrder = clientOrder;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
}
