package model;

/**
 * Created by Radu Bompa
 * 5/2/2018
 * Object that represent an order made by a client
 * An order can be in one of two statuses: IN_PROGRESS, FINISHED
 */
public class ClientOrder {
	private int id;
	private int customer;
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomer() {
		return customer;
	}

	public void setCustomer(int customer) {
		this.customer = customer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
