package model;

/**
 * Created by Radu Bompa
 * 5/2/2018
 * A class representing a product
 */
public class Product {
	private int id;
	private double stock;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getStock() {
		return stock;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
