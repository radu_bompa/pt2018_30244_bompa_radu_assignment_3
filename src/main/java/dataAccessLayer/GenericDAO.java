package dataAccessLayer;

import model.OrderItem;
import model.Product;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/3/2018
 */
public class GenericDAO {
	private Connection conn;

//	singleton
	private static GenericDAO instance = new GenericDAO();

	private GenericDAO() {
	}
	public static GenericDAO getInstance() {
		return instance;
	}

	/**
	 * Insert an object in the DB. The table is obtained from the object class name, and the query params from
	 * the object's methods
	 * @param obj - object to be inserted in the db
	 * @return - the id of the inserted row
	 */
	public int createObject(Object obj) {
		Connection conn = MySqlCon.getConnection();
		if (conn != null) {
			PreparedStatement stmt = null;
			try {
				String tableName = obj.getClass().getSimpleName();
				Method[] methods = obj.getClass().getMethods();
				String queryKeys = "";
				String queryValues = "";
				for (Method method : methods) {
					if (method.getName().contains("get") && !method.getName().equals("getClass")) {
						queryKeys += " " + method.getName().replace("get", "").toLowerCase() + ",";
						queryValues += " \"" + method.invoke(obj) + "\",";
					}
				}
				if (queryKeys.length() > 0) {
					queryKeys = queryKeys.substring(0, queryKeys.length() - 1);
				}
				if (queryValues.length() > 0) {
					queryValues = queryValues.substring(0, queryValues.length() - 1);
				}

				stmt = conn.prepareStatement("INSERT INTO " + tableName + " (" + queryKeys + ") VALUES (" + queryValues + ")", Statement.RETURN_GENERATED_KEYS);

				int productId = -1;
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					productId = rs.getInt(1);
				}
				rs.close();

				conn.close();
				return productId;
			} catch (SQLException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}

		}
		return -1;
	}

	/**
	 * Update an object in the DB. The table is obtained from the object class name, and the query params from
	 * the object's methods
	 * @param obj - object to be updated in the db
	 * @return - the id of the updated row
	 */
	public int updateObject(Object obj) {
		Connection conn = MySqlCon.getConnection();
		if (conn != null) {
			try {
				String tableName = obj.getClass().getSimpleName();
				Method[] methods = obj.getClass().getMethods();
				Integer id = -1;
				String queryParams = "";
				for (Method method : methods) {
					if (method.getName().equals("getId")) {
						id = Integer.parseInt(method.invoke(obj).toString());
					}
					if (method.getName().contains("get") && !method.getName().equals("getClass")) {
						queryParams += " " + method.getName().replace("get", "").toLowerCase() + " = \"" + method.invoke(obj) + "\",";
					}
				}
				if (queryParams.length() > 0) {
					queryParams = queryParams.substring(0, queryParams.length() - 1);
				}
				PreparedStatement stmt = conn.prepareStatement("UPDATE " + tableName + " SET " + queryParams + " where id=" + id);

				int productId = stmt.executeUpdate();
				conn.close();
				return productId;
			} catch (SQLException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}

		}
		return -1;
	}

	/**
	 * Retrieve an object from the DB. The table is obtained from the object class name,
	 * @param obj - object with the type and id to be retrieved from the db
	 * @return - the requested object
	 */
	public Object getObject(Object obj) {
		Connection conn = MySqlCon.getConnection();
		if (conn != null) {
			Statement stmt = null;
			try {
				String tableName = obj.getClass().getSimpleName();
				Method[] methods = obj.getClass().getMethods();
				methods = Arrays.stream(methods).sorted(Comparator.comparing(Method::getName)).collect(Collectors.toList()).toArray(new Method[methods.length]);
				Integer id = -1;
				for (Method method : methods) {
					if (method.getName().equals("getId")) {
						id = Integer.parseInt(method.invoke(obj).toString());
					}
				}
				stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("select * from " + tableName + " where id=" + id);
				if (rs.next()) {
					int rIndex = 1;
					for (Method method : methods) {
						if (method.getName().contains("set")) {
							if (method.getParameterTypes()[0].equals(String.class)) {
								method.invoke(obj, rs.getString(rIndex++));
							} else if (method.getParameterTypes()[0].equals(int.class) || method.getParameterTypes()[0].equals(Integer.class)) {
								method.invoke(obj, rs.getInt(rIndex++));
							} else if (method.getParameterTypes()[0].equals(double.class) || method.getParameterTypes()[0].equals(Double.class)) {
								method.invoke(obj, rs.getDouble(rIndex++));
							} else {
								method.invoke(obj, rs.getObject(rIndex++));
							}
						}
					}
					return obj;
				}
				conn.close();
			} catch (SQLException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Get all objects of a type from the DB
	 * @param obj - an object used to get the object type required
	 * @return - a list of all the requested object types
	 */
	public List<Object> getAllObjects(Object obj) {
		Connection conn = MySqlCon.getConnection();
		List<Object> objects = new LinkedList<>();
		if (conn != null) {
			String tableName = obj.getClass().getSimpleName();
			Method[] methods = obj.getClass().getMethods();
			methods = Arrays.stream(methods).sorted(Comparator.comparing(Method::getName)).collect(Collectors.toList()).toArray(new Method[methods.length]);
			try {
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("select * from " + tableName);
				while (rs.next()) {
					int rIndex = 1;
					Object resObject = obj.getClass().getConstructor().newInstance();
					resObject = obj.getClass().cast(resObject);
					for (Method method : methods) {
						if (method.getName().contains("set")) {
							if (method.getParameterTypes()[0].equals(String.class)) {
								method.invoke(resObject, rs.getString(method.getName().replace("set", "").toLowerCase()));
							} else if (method.getParameterTypes()[0].equals(int.class) || method.getParameterTypes()[0].equals(Integer.class)) {
								method.invoke(resObject, rs.getInt(method.getName().replace("set", "").toLowerCase()));
							} else if (method.getParameterTypes()[0].equals(double.class) || method.getParameterTypes()[0].equals(Double.class)) {
								method.invoke(resObject, rs.getDouble(method.getName().replace("set", "").toLowerCase()));
							} else {
								method.invoke(resObject, rs.getObject(rIndex++));
							}
						}
					}
					objects.add(resObject);
				}
				conn.close();
				return objects;
			} catch (SQLException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Remove an object of a type and id from the DB
	 * @param obj - the object to be removed
	 */
	public void deleteObject(Object obj) {
		try {
			Connection conn = MySqlCon.getConnection();
			if (conn != null) {
				String tableName = obj.getClass().getSimpleName();
				Method[] methods = obj.getClass().getMethods();
				Integer id = -1;
				for (Method method : methods) {
					if (method.getName().equals("getId")) {
						id = Integer.parseInt(method.invoke(obj).toString());
					}
				}
				Statement stmt = conn.createStatement();
				stmt.executeUpdate("delete from " + tableName + " where id = " + id);
				conn.close();
			}
		} catch (Exception e) {
			System.out.print(Arrays.toString(e.getStackTrace()));
		}
	}

	public List<Object> getObjects(Object obj, Object filter) {
		Connection conn = MySqlCon.getConnection();
		List<Object> objects = new LinkedList<>();
		if (conn != null) {
			try {
			String tableName = obj.getClass().getSimpleName();
			String filterName = filter.getClass().getSimpleName();
			String filterId = "";
			Method[] objMethods = obj.getClass().getMethods();
			Method[] filterMethods = filter.getClass().getMethods();
			for (Method method : filterMethods) {
				if (method.getName().contains("getId")) {
					filterId = method.invoke(filter) + "";
				}
			}
			objMethods = Arrays.stream(objMethods).sorted(Comparator.comparing(Method::getName)).collect(Collectors.toList()).toArray(new Method[objMethods.length]);

				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("select * from " + tableName.toLowerCase() + " where " + filterName + "=" + filterId);
				while (rs.next()) {
					int rIndex = 1;
					Object resObject = obj.getClass().getConstructor().newInstance();
					resObject = obj.getClass().cast(resObject);
					for (Method method : objMethods) {
						if (method.getName().contains("set")) {
							if (method.getParameterTypes()[0].equals(String.class)) {
								method.invoke(resObject, rs.getString(method.getName().replace("set", "").toLowerCase()));
							} else if (method.getParameterTypes()[0].equals(int.class) || method.getParameterTypes()[0].equals(Integer.class)) {
								method.invoke(resObject, rs.getInt(method.getName().replace("set", "").toLowerCase()));
							} else if (method.getParameterTypes()[0].equals(double.class) || method.getParameterTypes()[0].equals(Double.class)) {
								method.invoke(resObject, rs.getDouble(method.getName().replace("set", "").toLowerCase()));
							} else {
								method.invoke(resObject, rs.getObject(rIndex++));
							}
						}
					}
					objects.add(resObject);
				}
				conn.close();
				return objects;
			} catch (SQLException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
