package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class MySqlCon {
	/**
	 * Establishing a MySql connection to the database
	 * @return - a new Connection
	 */
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/tphw3", "root", "_Rangarock3");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
}