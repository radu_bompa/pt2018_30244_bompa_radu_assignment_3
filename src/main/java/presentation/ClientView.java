package presentation;

import businessLayer.ClientAdmin;
import model.Customer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class ClientView extends ViewController {

	private java.util.List<Customer> clientList;

	public ClientView() {
		super();

		JComponent panel = new JPanel();
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setAlignmentY(JPanel.TOP_ALIGNMENT);
		panel.setLayout(null);
		panel.setBounds(0, 0, 300, 600);

		Container pane = clientFrame.getContentPane();
		pane.add(panel);
		final GridBagConstraints c = new GridBagConstraints();

		ClientAdmin clientAdmin = new ClientAdmin();
		//      client id text
		JLabel clientIdLabel = new JLabel("Client id");
		JTextField clientIdText = new JTextField();
		clientIdText.setColumns(30);
		clientIdText.setEditable(false);

		clientIdLabel.setBounds(0, 30, 170, 20);
		panel.add(clientIdLabel);
		clientIdText.setBounds(0, 55, 100, 20);
		panel.add(clientIdText);

		//      client name text
		JLabel clientNameLabel = new JLabel("Client name");
		JTextField clientNameText = new JTextField();
		clientNameText.setColumns(30);

		clientNameLabel.setBounds(0, 75, 170, 20);
		panel.add(clientNameLabel);
		clientNameText.setBounds(0, 100, 100, 20);
		panel.add(clientNameText);

//		all clients table

		JTable clientsTable = createTable(clientAdmin.getAllClients(new Customer()));

		clientsTable.getSelectionModel().addListSelectionListener(event -> {
			if (clientsTable.getSelectedRow() >= 0 && clientsTable.getValueAt(clientsTable.getSelectedRow(), 1) != null && clientsTable.getValueAt(clientsTable.getSelectedRow(), 1) != "") {
				Customer customer = new Customer();
				customer.setId(Integer.parseInt(clientsTable.getValueAt(clientsTable.getSelectedRow(), 1).toString()));
				customer = clientAdmin.getClient(customer);
				if (customer != null) {
					clientIdText.setText(customer.getId() + "");
					clientNameText.setText(customer.getName());
				}
			} else {
				clientNameText.setText("");
				clientIdText.setText("");
			}
		});

		JScrollPane sp = new JScrollPane(clientsTable);
		sp.setBounds(0, 220, 300, 300);
		panel.add(sp);

//      add, edit, delete buttons
		JButton addClientButton = new JButton("Add client");
		addClientButton.addActionListener(e -> {
			try {
				Customer customer = new Customer();
				customer.setName(clientNameText.getText());
				int id = clientAdmin.addClient(customer);
				((DefaultTableModel) clientsTable.getModel()).addRow(new Object[]{customer.getName(), id + ""});
				panel.repaint();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		addClientButton.setBounds(150, 30, 100, 30);
		panel.add(addClientButton);

		JButton editClientButton = new JButton("Edit client");
		editClientButton.addActionListener(e -> {
			try {
				int selectedRow = clientsTable.getSelectedRow();
				if (clientsTable.getValueAt(selectedRow, 1) != "") {
					Customer customer = new Customer();
					customer.setId(Integer.parseInt(clientsTable.getValueAt(selectedRow, 1).toString()));
					customer.setName(clientNameText.getText());
					clientAdmin.editClient(customer);
					clientsTable.getModel().setValueAt(customer.getName(), selectedRow, 0);
					panel.repaint();
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		editClientButton.setBounds(150, 70, 100, 30);
		panel.add(editClientButton);

		JButton deleteClientButton = new JButton("Delete client");
		deleteClientButton.addActionListener(e -> {
			try {
				int selectedRow = clientsTable.getSelectedRow();
				if (selectedRow >= 0) {
					clientsTable.clearSelection();
					clientsTable.revalidate();
					clientsTable.repaint();
					Customer customer = new Customer();
					customer.setId(Integer.parseInt(clientsTable.getValueAt(selectedRow, 1).toString()));
					((DefaultTableModel) clientsTable.getModel()).removeRow(clientsTable.convertRowIndexToModel(selectedRow));
					clientAdmin.deleteClient(customer);
				}
			} catch (Exception ex) {
				System.out.println("Error: " + ex.getMessage());
			}
		});

		deleteClientButton.setBounds(150, 110, 100, 30);
		panel.add(deleteClientButton);


	}
}
