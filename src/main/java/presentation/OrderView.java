package presentation;

import businessLayer.ClientAdmin;
import businessLayer.OrderProcessing;
import businessLayer.WarehouseAdmin;
import model.ClientOrder;
import model.Customer;
import model.OrderItem;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class OrderView extends ViewController {
	protected static final JFrame f = new JFrame();//fereastra principala

	public OrderView() {
		super();

		JComponent panel = new JPanel();
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setAlignmentY(JPanel.TOP_ALIGNMENT);
		panel.setLayout(null);
		panel.setBounds(0, 0, 300, 600);

		Container pane = orderFrame.getContentPane();
		pane.add(panel);
		final GridBagConstraints c = new GridBagConstraints();

		OrderProcessing orderAdmin = new OrderProcessing();
		ClientAdmin clientAdmin = new ClientAdmin();
		//  select client

		String[] clientIds = getClientIds(clientAdmin.getAllClients(new Customer()));
		if (clientIds != null) {
			clientIds[clientIds.length - 1] = "";
		}
		JLabel clientIdLabel = new JLabel("Client id");
		JComboBox clientIdSelect = new JComboBox(clientIds);
		clientIdSelect.setSelectedIndex(clientIds.length - 1);
		clientIdSelect.addActionListener(e -> {
			try {
				if (clientIdSelect.getSelectedItem() != null && clientIdSelect.getSelectedItem().toString() != "") {
					//		select order
					Customer customer = new Customer();
					customer.setId(Integer.parseInt(clientIdSelect.getSelectedItem().toString()));

					JTable ordersTable = createTable(orderAdmin.getAllOrders(new ClientOrder(), customer));

					Component[] components2 = panel.getComponents();
					for (Component component : components2) {
						if (component.getName() != null && (component.getName().equals("addOrderButton") ||
								component.getName().equals("orderItemsTable"))) {
							panel.remove(component);
							panel.repaint();
						}
					}
					JButton addOrderButton = new JButton("Add order");
					ActionListener addOrderListener = new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							try {
								ClientOrder order = new ClientOrder();
								order.setCustomer(Integer.parseInt(clientIdSelect.getSelectedItem().toString()));
								order.setStatus("IN_PROGRESS");
								orderAdmin.addOrder(order);
								JTable ordersTable2 = createTable(orderAdmin.getAllOrders(new ClientOrder(), customer));
								JScrollPane sp = new JScrollPane(ordersTable2);
								sp.setName("ordersTable");
								sp.setBounds(0, 80, 300, 100);
								panel.add(sp);
								panel.repaint();
							} catch (Exception ex) {
								System.out.print("Error: " + ex.getMessage());
							}
						}
					};
					addOrderButton.addActionListener(addOrderListener);

					addOrderButton.setBounds(180, 40, 100, 30);
					addOrderButton.setName("addOrderButton");
					panel.add(addOrderButton);
					panel.repaint();

					if (ordersTable != null) {
						Component[] components3 = panel.getComponents();
						for (Component component : components2) {
							if (component.getName() != null && (component.getName().equals("ordersTable") ||
												component.getName().equals(""))) {
								panel.remove(component);
								panel.repaint();
							}
						}


						//		finalize order button

						JButton finalizeOrderButton = new JButton("Finalize order");
						finalizeOrderButton.setName("finalizeOrderButton");
						finalizeOrderButton.addActionListener(ev4 -> {
							try {
								if (ordersTable.getSelectedRow() >= 0 && ordersTable.getValueAt(ordersTable.getSelectedRow(), 1) != null && ordersTable.getValueAt(ordersTable.getSelectedRow(), 1) != "") {

									//  view products in order
									ClientOrder order = new ClientOrder();
									order.setId(Integer.parseInt(ordersTable.getValueAt(ordersTable.getSelectedRow(), 1).toString()));
									order.setCustomer(Integer.parseInt(ordersTable.getValueAt(ordersTable.getSelectedRow(), 0).toString()));
									order.setStatus("FINISHED");
									orderAdmin.editOrder(order);
									ordersTable.getModel().setValueAt("FINISHED", ordersTable.getSelectedRow(), 2);

									List<OrderItem> orderItems = orderAdmin.getAllOrderItems(new OrderItem(), order);
									String comanda = "";
									for (OrderItem orderItem : orderItems) {
										comanda += "Produs: " + orderItem.getProductId() + ", cantitate: " +  orderItem.getQuantity() + "\n";
									}

									BufferedWriter writer = new BufferedWriter(new FileWriter("order_" + order.getId() + ".txt"));
									writer.write(comanda);

									writer.close();
								}
							} catch (Exception ex) {
								System.out.print("Error: " + ex.getMessage());
							}
						});

						finalizeOrderButton.setBounds(150, 520, 100, 30);
						panel.add(finalizeOrderButton);

						ordersTable.setName("ordersTable");
						ordersTable.getSelectionModel().addListSelectionListener(event -> {
							if (ordersTable.getSelectedRow() >= 0 && ordersTable.getValueAt(ordersTable.getSelectedRow(), 1) != null && ordersTable.getValueAt(ordersTable.getSelectedRow(), 1) != "") {

								Component[] components1 = panel.getComponents();
								for (Component component : components1) {
									if (component.getName() != null && component.getName().equals("orderItemsTable")) {
										panel.remove(component);
										panel.repaint();
									}
								}

								//  view products in order
								ClientOrder order = new ClientOrder();
								order.setId(Integer.parseInt(ordersTable.getValueAt(ordersTable.getSelectedRow(), 1).toString()));
								JTable orderItemsTable = createTable(orderAdmin.getAllOrderItems(new OrderItem(), order));

//								product id
								WarehouseAdmin productAdmin = new WarehouseAdmin();
								List<Product> products = productAdmin.getAllProducts(new Product());
								String[] productIds = getProductIds(products);
								JComboBox productIdSelect = new JComboBox(productIds);
								productIdSelect.setName("productIdSelect");
								productIdSelect.setSelectedIndex(productIds.length - 1);

								JLabel productIdLabel = new JLabel("Product");
								productIdLabel.setBounds(0, 460, 100, 20);
								panel.add(productIdLabel);
								productIdSelect.setBounds(100, 460, 170, 20);
								panel.add(productIdSelect);

								//      product quantity text
								JLabel quantityLabel = new JLabel("Quantity");
								JTextField quantityText = new JTextField();
								quantityText.setName("quantityText");
								quantityText.setColumns(30);

								quantityLabel.setBounds(0, 420, 100, 20);
								panel.add(quantityLabel);
								quantityText.setBounds(100, 420, 100, 20);
								panel.add(quantityText);

								if (ordersTable.getValueAt(ordersTable.getSelectedRow(), 2).equals("IN_PROGRESS")) {
									JButton addOrderItemButton = new JButton("Add item");
									addOrderItemButton.addActionListener(ev1 -> {
										try {
											OrderItem orderItem = new OrderItem();
											orderItem.setClientOrder(Integer.parseInt(ordersTable.getValueAt(ordersTable.getSelectedRow(), 1).toString()));
											orderItem.setProductId(Integer.parseInt(productIdSelect.getSelectedItem().toString()));
											orderItem.setQuantity(Double.parseDouble(quantityText.getText()));

											Product product = new Product();
											product.setId(Integer.parseInt(productIdSelect.getSelectedItem().toString()));
											product = productAdmin.getProduct(product);
											if (product.getStock() >= orderItem.getQuantity()) {
												orderAdmin.addOrderItem(orderItem);
												product.setStock(product.getStock() - orderItem.getQuantity());
												productAdmin.editProduct(product);
												JTable orderItemsTable2 = createTable(orderAdmin.getAllOrderItems(new OrderItem(), order));
												JScrollPane sp = new JScrollPane(orderItemsTable2);
												sp.setBounds(0, 220, 300, 150);
												sp.setName("orderItemsTable");
												panel.add(sp);
												panel.repaint();
											} else {
												JOptionPane.showMessageDialog(null, "Under stock!");
											}
										} catch (Exception ex) {
											JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
										}
									});

									addOrderItemButton.setBounds(10, 375, 100, 30);
									addOrderItemButton.setName("addOrderItemButton");
									panel.add(addOrderItemButton);
									panel.repaint();
								} else {
									Component[] components = panel.getComponents();
									for (Component component : components) {
										if (component.getName() != null && (component.getName().equals("addOrderItemButton") ||
												component.getName().equals("productIdSelect") ||
												component.getName().equals("quantityText"))) {
											panel.remove(component);
											panel.repaint();
										}
									}
								}

								if (orderItemsTable != null) {

									orderItemsTable.getSelectionModel().addListSelectionListener(ev -> {
										if (orderItemsTable.getSelectedRow() >= 0 && orderItemsTable.getValueAt(orderItemsTable.getSelectedRow(), 1) != null && orderItemsTable.getValueAt(orderItemsTable.getSelectedRow(), 1) != "") {

											//      add, delete order item buttons
											if (ordersTable.getValueAt(ordersTable.getSelectedRow(), 2) != null &&
													ordersTable.getValueAt(ordersTable.getSelectedRow(), 2).equals("IN_PROGRESS")) {

												JButton deleteOrderItemButton = new JButton("Delete order");
												deleteOrderItemButton.addActionListener(ev2 -> {
													try {
														if (orderItemsTable.getSelectedRow() >= 0) {
															OrderItem orderItem = new OrderItem();
															orderItem.setId(Integer.parseInt(orderItemsTable.getValueAt(orderItemsTable.getSelectedRow(), 1).toString()));
															orderAdmin.deleteOrderItem(orderItem);
															((DefaultTableModel) orderItemsTable.getModel()).removeRow(orderItemsTable.convertRowIndexToModel(orderItemsTable.getSelectedRow()));
															Component[] components = panel.getComponents();
															for (Component component : components) {
																if (component.getName() != null && (component.getName().equals("addOrderItemButton") || component.getName().equals("deleteOrderItemButton"))) {
																	panel.remove(component);
																	panel.repaint();
																}
															}
															panel.repaint();
														}
													} catch (Exception ex) {
														JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
													}
												});

												deleteOrderItemButton.setBounds(110, 375, 80, 30);
												deleteOrderItemButton.setName("deleteOrderItemButton");
												panel.add(deleteOrderItemButton);

												panel.repaint();
											}
										} else {

										}
									});

									JScrollPane sp = new JScrollPane(orderItemsTable);
									sp.setBounds(0, 220, 300, 150);
									sp.setName("orderItemsTable");
									panel.add(sp);
								} else {
									Component[] components = panel.getComponents();
									for (Component component : components) {
										if (component.getName() != null && (component.getName().equals("orderItemsTable") ||
												component.getName().equals("deleteOrderItemButton"))) {
											panel.remove(component);
											panel.repaint();
										}
									}
								}
							} else {
								Component[] components = panel.getComponents();
								for (Component component : components) {
									if (component.getName() != null && component.getName().equals("orderItemsTable")) {
										panel.remove(component);
										panel.repaint();
									}
								}
							}
						});

						JScrollPane sp = new JScrollPane(ordersTable);
						sp.setName("ordersTable");
						sp.setBounds(0, 80, 300, 100);
						panel.add(sp);

						//      add, delete order buttons
						addOrderButton = new JButton("Add order");
						addOrderButton.removeActionListener(addOrderListener);
						addOrderButton.addActionListener(ev1 -> {
							try {
								ClientOrder order = new ClientOrder();
								order.setCustomer(Integer.parseInt(clientIdSelect.getSelectedItem().toString()));
								order.setStatus("IN_PROGRESS");
								int id = orderAdmin.addOrder(order);
								((DefaultTableModel) ordersTable.getModel()).addRow(new Object[]{id + "", order.getStatus(), order.getCustomer() + ""});
								panel.repaint();
							} catch (Exception ex) {
								JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
							}
						});

						addOrderButton.setBounds(180, 40, 100, 30);
						addOrderButton.setName("addOrderButton");
						panel.add(addOrderButton);

						JButton deleteOrderButton = new JButton("Delete order");
						deleteOrderButton.addActionListener(ev2 -> {
							try {
								if (ordersTable.getSelectedRow() >= 0) {
									ClientOrder clientOrder = new ClientOrder();
									clientOrder.setId(Integer.parseInt(ordersTable.getValueAt(ordersTable.getSelectedRow(), 1).toString()));
									orderAdmin.deleteOrder(clientOrder);
									((DefaultTableModel) ordersTable.getModel()).removeRow(ordersTable.convertRowIndexToModel(ordersTable.getSelectedRow()));
									Component[] components = panel.getComponents();
									for (Component component : components) {
										if (component.getName() != null && component.getName().equals("orderItemsTable")) {
											panel.remove(component);
											panel.repaint();
											break;
										}
									}
									panel.repaint();
								}
							} catch (Exception ex) {
								JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
							}
						});

						deleteOrderButton.setBounds(150, 185, 100, 30);
						deleteOrderButton.setName("deleteOrderButton");
						panel.add(deleteOrderButton);

						panel.repaint();
					} else {
						Component[] components = panel.getComponents();
						for (Component component : components) {
							if (component.getName() != null && (component.getName().equals("ordersTable")
									|| component.getName().equals("deleteOrderButton"))) {
								panel.remove(component);
								panel.repaint();
							}
						}
					}

				} else {
					Component[] components = panel.getComponents();
					for (Component component : components) {
						if (component.getName() != null && (component.getName().equals("ordersTable")
								|| component.getName().equals("deleteOrderButton")
								|| component.getName().equals("addOrderButton"))) {
							panel.remove(component);
							panel.repaint();
						}
					}
				}
			} catch (Exception ex) {
				System.out.print("Error: " + ex.getMessage());
			}
		});

		clientIdLabel.setBounds(0, 30, 170, 20);
		panel.add(clientIdLabel);

		clientIdSelect.setBounds(0, 55, 170, 20);
		panel.add(clientIdSelect);

	}

	private String[] getProductIds(List<Product> products) {
		java.util.List<String> productIds = new LinkedList<String>();
		if (products != null) {
			for (Product product : products) {
				productIds.add(product.getId() + "");
			}
			return productIds.toArray(new String[productIds.size() + 1]);
		} else {
			return new String[1];
		}
	}

	private String[] getOrderIds(List<ClientOrder> allOrders) {
		java.util.List<String> orderIds = new LinkedList<String>();
		if (allOrders != null) {
			for (ClientOrder order : allOrders) {
				orderIds.add(order.getId() + "");
			}
			return orderIds.toArray(new String[orderIds.size() + 1]);
		} else {
			return new String[1];
		}
	}

	private static String[] getClientIds(java.util.List<Customer> allClients) {
		java.util.List<String> clientIds = new LinkedList<String>();
		if (allClients != null) {
			for (Customer customer : allClients) {
				clientIds.add(customer.getId() + "");
			}
			return clientIds.toArray(new String[clientIds.size() + 1]);
		} else {
			return new String[1];
		}
	}
}