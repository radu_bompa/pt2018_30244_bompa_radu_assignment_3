package presentation;

import businessLayer.ClientAdmin;
import model.Customer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class ViewController extends JPanel{
	protected static final JFrame clientFrame = new JFrame("Clienti");//fereastra clienti
	protected static final JFrame productFrame = new JFrame("Produse");//fereastra produse
	protected static final JFrame orderFrame = new JFrame("Comenzi");//fereastra comenzi

	private java.util.List<Customer> clientList;

	public ViewController() {
		super();
	}

	/**
	 * Generic method to create a table
	 * @param objects - the objects from which we extract the table headers and data
	 * @return - a new table
	 */
	protected JTable createTable(java.util.List<?> objects) {
		String[] tableHeaders = null;
		String[][] tableValues = null;
		if (objects != null && !objects.isEmpty()) {
			int propCount = 0;
			Method[] methods = objects.get(0).getClass().getMethods();
			methods = Arrays.stream(methods).sorted(Comparator.comparing(Method::getName)).collect(Collectors.toList()).toArray(new Method[methods.length]);
			List<Method> getMethods = new LinkedList<>();
			for (Method method : methods) {
				if (method.getName().contains("get") && !method.getName().contains("getClass")) {
					propCount++;
					getMethods.add(method);
				}
			}
			tableHeaders = new String[propCount];
			tableValues = new String[objects.size()][propCount];
			for (int i = 0; i < getMethods.size(); i++) {
				Method method = getMethods.get(i);
				if (method.getName().contains("get")) {
					method.setAccessible(true);
					tableHeaders[i] = method.getName().replace("get", "");
				}
			}

			for (int i1 = 0; i1 < objects.size(); i1++) {
				Object obj = objects.get(i1);
				for (int i = 0; i < getMethods.size(); i++) {
					Method method = getMethods.get(i);
					if (method.getName().contains("get")) {
						method.setAccessible(true);
						try {
							if (method.invoke(obj) != null) {
								tableValues[i1][i] = method.invoke(obj) + "";
							} else {
								tableValues[i1][i] = "-";
							}
						} catch (IllegalAccessException | InvocationTargetException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		if (tableValues != null) {
			return new JTable(new DefaultTableModel(tableValues, tableHeaders));
		} else {
			return null;
		}
	}

	/**
	 * Metoda care afiseaza fereastra principala
	 */
	private static void createAndShowGUI() {
		//Add content to the window.
		clientFrame.setLayout(null);
		clientFrame.add(new ClientView());
		clientFrame.setSize(new Dimension(300, 600));
		//Display the window.
//        clientFrame.pack();
		clientFrame.setVisible(true);//making the frame visible


		//Add content to the window.
		productFrame.setLayout(null);
		productFrame.add(new ProductView());
		productFrame.setSize(new Dimension(300, 600));
		//Display the window.
//        clientFrame.pack();
		productFrame.setVisible(true);//making the frame visible


		//Add content to the window.
		orderFrame.setLayout(null);
		orderFrame.add(new OrderView());
		orderFrame.setSize(new Dimension(300, 600));
		//Display the window.
//        clientFrame.pack();
		orderFrame.setVisible(true);//making the frame visible
	}

	public static void main(String[] args) {

		//Schedule a job for the event dispatch thread:
		//creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				//Turn off metal's use of bold fonts
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();
			}
		});
	}
}