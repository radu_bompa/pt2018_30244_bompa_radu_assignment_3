package presentation;

import businessLayer.WarehouseAdmin;
import model.Product;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class ProductView extends ViewController {
	protected static final JFrame f = new JFrame();//fereastra principala


	public ProductView() {
		super();

		JComponent panel = new JPanel();
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setAlignmentY(JPanel.TOP_ALIGNMENT);
		panel.setLayout(null);
		panel.setBounds(0, 0, 300, 600);

		Container pane = productFrame.getContentPane();
		pane.add(panel);
		final GridBagConstraints c = new GridBagConstraints();

		WarehouseAdmin productAdmin = new WarehouseAdmin();
		//      product id text
		JLabel productIdLabel = new JLabel("Id");
		JTextField productIdText = new JTextField();
		productIdText.setColumns(30);
		productIdText.setEditable(false);

		productIdLabel.setBounds(0, 30, 170, 20);
		panel.add(productIdLabel);
		productIdText.setBounds(0, 55, 100, 20);
		panel.add(productIdText);

		//      product name text
		JLabel productNameLabel = new JLabel("Name");
		JTextField productNameText = new JTextField();
		productNameText.setColumns(30);

		productNameLabel.setBounds(0, 75, 170, 20);
		panel.add(productNameLabel);
		productNameText.setBounds(0, 100, 100, 20);
		panel.add(productNameText);

		//      product stock text
		JLabel productStockLabel = new JLabel("Stock");
		JTextField productStockText = new JTextField();
		productStockText.setColumns(30);

		productStockLabel.setBounds(0, 125, 170, 20);
		panel.add(productStockLabel);
		productStockText.setBounds(0, 150, 100, 20);
		panel.add(productStockText);

//		all products table

		java.util.List<Product> productList;
		if (productAdmin.getAllProducts(new Product()) == null || productAdmin.getAllProducts(new Product()).isEmpty()) {
			productList = new LinkedList<>();
			productList.add(new Product());
		} else {
			productList = productAdmin.getAllProducts(new Product());
		}
		JTable productsTable = createTable(productList);

		productsTable.getSelectionModel().addListSelectionListener(event -> {
			if (productsTable.getSelectedRow() >= 0 && productsTable.getValueAt(productsTable.getSelectedRow(), 1) != null && productsTable.getValueAt(productsTable.getSelectedRow(), 1) != "") {
				Product product = new Product();
				product.setId(Integer.parseInt(productsTable.getValueAt(productsTable.getSelectedRow(), 1).toString()));
				product = productAdmin.getProduct(product);
				if (product != null) {
					productIdText.setText(product.getId() + "");
					productNameText.setText(product.getName());
					productStockText.setText(product.getStock() + "");
				}
			} else {
				productNameText.setText("");
				productStockText.setText("");
				productIdText.setText("");
			}
		});

		JScrollPane sp = new JScrollPane(productsTable);
		sp.setBounds(0, 220, 300, 300);
		panel.add(sp);

		//      add, edit, delete buttons
		JButton addProductButton = new JButton("Add product");
		addProductButton.addActionListener(e -> {
			try {
				Product product = new Product();
				product.setName(productNameText.getText());
				product.setStock(Double.parseDouble(productStockText.getText()));
				int id = productAdmin.addProduct(product);
				((DefaultTableModel) productsTable.getModel()).addRow(new Object[]{product.getName(), id+"", product.getStock()+""});
				panel.repaint();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		addProductButton.setBounds(150, 30, 100, 30);
		panel.add(addProductButton);

		JButton editProductButton = new JButton("Edit product");
		editProductButton.addActionListener(e -> {
			try {
				int selectedRow = productsTable.getSelectedRow();
				if (productsTable.getValueAt(selectedRow, 1) != "") {
					Product product = new Product();
					product.setId(Integer.parseInt(productsTable.getValueAt(selectedRow, 1).toString()));
					product.setName(productNameText.getText());
					product.setStock(Double.parseDouble(productStockText.getText()));
					productAdmin.editProduct(product);
					productsTable.getModel().setValueAt(product.getName(), selectedRow, 0);
					productsTable.getModel().setValueAt(product.getStock() + "", selectedRow, 2);
					panel.repaint();
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		editProductButton.setBounds(150, 70, 100, 30);
		panel.add(editProductButton);

		JButton deleteProductButton = new JButton("Delete product");
		deleteProductButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				super.mouseReleased(e);
				try {
					int selectedRow = productsTable.getSelectedRow();
					if (selectedRow >= 0) {
						productsTable.clearSelection();
						productsTable.revalidate();
						productsTable.repaint();
						Product product = new Product();
						product.setId(Integer.parseInt(productsTable.getValueAt(selectedRow, 1).toString()));
						((DefaultTableModel)productsTable.getModel()).removeRow(productsTable.convertRowIndexToModel(selectedRow));
						productAdmin.deleteProduct(product);
					}
				} catch (Exception ex) {
					System.out.print("Error: " + ex.getMessage());
				}
			}
		});

		deleteProductButton.setBounds(150, 110, 100, 30);
		panel.add(deleteProductButton);

	}


	protected static String[] getProductIds(java.util.List<Product> allProducts) {
		java.util.List<String> productIds = new LinkedList<String>();
		if (allProducts != null) {
			for (Product product : allProducts) {
				productIds.add(product.getId() + "");
			}
			return productIds.toArray(new String[productIds.size() + 1]);
		} else {
			return new String[1];
		}
	}

}
