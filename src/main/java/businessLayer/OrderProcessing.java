package businessLayer;

import dataAccessLayer.GenericDAO;
import model.ClientOrder;
import model.Customer;
import model.OrderItem;
import model.Product;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class OrderProcessing {
	private GenericDAO orderDAO = GenericDAO.getInstance();

	/**
	 * Create a new order
	 * @param order - order to be created
	 * @return - the id of the newly created order
	 */
	public int addOrder(ClientOrder order) {
		return orderDAO.createObject(order);
	}

	/**
	 * Modify an existing order
	 * @param order - order to be modified
	 * @return - the id of the updated order
	 */
	public int editOrder(ClientOrder order) {
		return orderDAO.updateObject(order);
	}

	/**
	 * Get an order by id
	 * @param order - the order with the id to be retrieved
	 * @return - the requested order
	 */
	public ClientOrder getOrder(ClientOrder order) {
		return (ClientOrder) orderDAO.getObject(order);
	}

	/**
	 * Retrieve all the orders for a client
	 * @param order - order object type
	 * @param customer - client for which we retrieve the orders
	 * @return - all the orders of that client
	 */
	public List<ClientOrder> getAllOrders(ClientOrder order, Customer customer) {
		List<Object> objects = orderDAO.getObjects(order, customer);
		return objects.stream().map(o -> (ClientOrder) o).collect(Collectors.toList());
	}

	/**
	 * Remove an order
	 * @param order - order to be removed
	 */
	public void deleteOrder(ClientOrder order) {
		orderDAO.deleteObject(order);
	}

	/**
	 * Add a new order item
	 * @param orderItem - item to be added
	 * @return - the id of the newly created order item
	 */
	public int addOrderItem(OrderItem orderItem) {
		return orderDAO.createObject(orderItem);
	}

	/**
	 * Modify an existing order item
	 * @param orderItem - item to be modified
	 * @return - the id of the updated order item
	 */
	public int editOrderItem(OrderItem orderItem) {
		return orderDAO.updateObject(orderItem);
	}

	/**
	 * Retrieve an order item
	 * @param orderItem - item with the id to be retrieved
	 * @return - the requested item
	 */
	public OrderItem getOrderItem(OrderItem orderItem) {
		return (OrderItem) orderDAO.getObject(orderItem);
	}

	/**
	 * Retrieve all the items for a certain order
	 * @param orderItem - item object type
	 * @param order - the order for which we retrieve the items
	 * @return - all the items for the specific order
	 */
	public List<OrderItem> getAllOrderItems(OrderItem orderItem, ClientOrder order) {
		List<Object> objects = orderDAO.getObjects(orderItem, order);
		return objects.stream().map(o -> (OrderItem) o).collect(Collectors.toList());
	}

	/**
	 * Remove an item from an order
	 * @param orderItem - item to be removed
	 */
	public void deleteOrderItem(OrderItem orderItem) {
		orderDAO.deleteObject(orderItem);
	}
}
