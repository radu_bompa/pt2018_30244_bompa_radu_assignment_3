package businessLayer;

import dataAccessLayer.GenericDAO;
import model.Customer;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class ClientAdmin {
	private GenericDAO clientDAO = GenericDAO.getInstance();

	/**
	 * Create a new client
	 * @param customer - client to be created
	 * @return - the id of the new inserted client
	 */
	public int addClient(Customer customer) {
		return clientDAO.createObject(customer);
	}

	/**
	 * Get a client by id
	 * @param client - client with the id to be retrieved
	 * @return - the requested client or null
	 */
	public Customer getClient(Customer client) {
		return (Customer) clientDAO.getObject(client);
	}

	/**
	 * Get all clients
	 * @param client - object used to get the type of the requested objects
	 * @return - a list of all clients in db
	 */
	public List<Customer> getAllClients(Customer client) {
		List<Object> objects = clientDAO.getAllObjects(client);
		return objects.stream().map(o -> (Customer)o).collect(Collectors.toList());
	}

	/**
	 * Modify a client parameters (except id)
	 * @param customer - client with the parameters to be updated
	 * @return - the id of the client
	 */
	public int editClient(Customer customer) {
		return clientDAO.updateObject(customer);
	}

	/**
	 * Remove a client
	 * @param client - client to be removed
	 */
	public void deleteClient(Customer client) {
		clientDAO.deleteObject(client);
	}
}
