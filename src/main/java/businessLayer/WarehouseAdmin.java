package businessLayer;

import dataAccessLayer.GenericDAO;
import model.Product;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class WarehouseAdmin {
	private GenericDAO productDAO = GenericDAO.getInstance();

	/**
	 * Create a new product
	 * @param product - to be created
	 * @return - the id of the newly created product
	 */
	public int addProduct(Product product) {
		return productDAO.createObject(product);
	}

	/**
	 * Retrieve a product by id
	 * @param product - with the id to be retrieved
	 * @return - the requested product
	 */
	public Product getProduct(Product product) {
		return (Product) productDAO.getObject(product);
	}

	/**
	 * Get all the products in the DB
	 * @param product - the object type
	 * @return - all the products
	 */
	public List<Product> getAllProducts(Product product) {
		List<Object> objects = productDAO.getAllObjects(product);
		return objects.stream().map(o -> (Product)o).collect(Collectors.toList());
	}

	/**
	 * Update a chosen product
	 * @param product - to be modified
	 * @return - the id of the modified product
	 */
	public int editProduct(Product product) {
		return productDAO.updateObject(product);
	}

	/**
	 * Remove a product
	 * @param product - to be deleted
	 */
	public void deleteProduct(Product product) {
		productDAO.deleteObject(product);
	}
}
